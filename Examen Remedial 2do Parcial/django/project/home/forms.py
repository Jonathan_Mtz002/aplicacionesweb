from pyexpat import model
from django import forms
from . import models


#--------------------------------------------------------------
#Jugadores

class CreateJugador(forms.ModelForm):
    class Meta:
        model = models.Jugador
        fields = [
            'nombre',
            'posicion',
            'equipo'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'posicion' : forms.TextInput(attrs={'class': 'form-control'}),
            'equipo': forms.Select(attrs={'class': 'form-control'})
            
            
        }
        
class UpdateJugador(forms.ModelForm):
    class Meta:
        model = models.Jugador
        fields =[
            'nombre',
            'posicion',
            'equipo'
        ]
        
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'posicion' : forms.TextInput(attrs={'class': 'form-control'}),
            'equipo': forms.Select(attrs={'class': 'form-control'})
        }

#--------------------------------------------------------------

#--------------------------------------------------------------
#Equipos

class CreateEquipos(forms.ModelForm):
    class Meta:
        model = models.Equipo
        fields = [
            'nombre',
            'estadio',
            'propietario'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'estadio' : forms.Select(attrs={'class': 'form-control'}),
            'propietario': forms.Select(attrs={'class': 'form-control'})
            
            
        }
        
class UpdateEquipos(forms.ModelForm):
    class Meta:
        model = models.Equipo
        fields = [
            'nombre',
            'estadio',
            'propietario'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'estadio' : forms.Select(attrs={'class': 'form-control'}),
            'propietario': forms.Select(attrs={'class': 'form-control'})
            
            
        }

#--------------------------------------------------------------

#--------------------------------------------------------------
#Estadios

class CreateEstadios(forms.ModelForm):
    class Meta:
        model = models.Estadio
        fields = [
            'nombre',
            'capacidad'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'capacidad' : forms.NumberInput(attrs={'class': 'form-control'}),
            
            
        }
        
class UpdateEstadios(forms.ModelForm):
    class Meta:
        model = models.Estadio
        fields = [
            'nombre',
            'capacidad'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'capacidad' : forms.NumberInput(attrs={'class': 'form-control'}),
        }

#--------------------------------------------------------------

#--------------------------------------------------------------
#Owner

class CreateOwner(forms.ModelForm):
    class Meta:
        model = models.Owner
        fields = [
            'nombre',
            'fortuna'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'owner' : forms.TextInput(attrs={'class': 'form-control'}),
            
            
        }
        
class UpdateOwner(forms.ModelForm):
    class Meta:
        model = models.Owner
        fields = [
            'nombre',
            'fortuna'
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'owner' : forms.TextInput(attrs={'class': 'form-control'}),
            
            
        }
#--------------------------------------------------------------