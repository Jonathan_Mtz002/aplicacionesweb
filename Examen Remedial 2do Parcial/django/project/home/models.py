from django.db import models

# Create your models here.

class Jugador(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    posicion = models.CharField(max_length=100)
    equipo = models.ForeignKey('Equipo', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.nombre} - {self.posicion}"
    
class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    estadio = models.ForeignKey('Estadio', on_delete=models.CASCADE)
    propietario = models.ForeignKey('Owner', on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre
    
class Estadio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    capacidad = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre
    
class Owner(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    fortuna = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.nombre