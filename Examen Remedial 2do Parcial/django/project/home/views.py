from pyexpat import model
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.cache import never_cache
from django.contrib import messages
from . import forms, models
from django.contrib.auth import authenticate


# Create your views here.

def index(request):
    return render(request, 'home/index.html', {})


#----------------------------------------------------------
#Jugadores
class jugadores(generic.CreateView):
   template_name = 'home/jugadores.html'
   model = models.Jugador
   form_class = forms.CreateJugador
   success_url = '/jugadores/'
   
   def get(self, request):
       jugadores = models.Jugador.objects.all()
       context = {
           'Jugador': jugadores,
           'form': self.form_class
       }
       
       return render(request, self.template_name, context)

class editarJugador(generic.UpdateView):
    
    template_name = 'home/editarJugador.html'
    model = models.Jugador
    form_class = forms.UpdateJugador
    
    
    success_url = reverse_lazy("jugadores")

class eliminarJugador(generic.DeleteView):
    
    def get(self, request, pk):
        jugadores = models.Jugador.objects.get(id=pk)
        jugadores.delete()
        jugadores = models.Jugador.objects.all()

        return redirect('jugadores/')

#----------------------------------------------------------



#----------------------------------------------------------
#Equipos
class equipos(generic.CreateView):
   template_name = 'home/equipos.html'
   model = models.Equipo
   form_class = forms.CreateEquipos
   success_url = '/equipos/'
   
   def get(self, request):
       equipos = models.Equipo.objects.all()
       context = {
           'Equipo': equipos,
           'form': self.form_class
       }
       
       return render(request, self.template_name, context)

class editarEquipos(generic.UpdateView):
    
    template_name = 'home/editarEquipos.html'
    model = models.Equipo
    form_class = forms.UpdateEquipos
    
    
    success_url = reverse_lazy("equipos")

class eliminarEquipos(generic.DeleteView):
    
    def get(self, request, pk):
        Equipos = models.Equipo.objects.get(id=pk)
        Equipos.delete()
        Equipos = models.Jugador.objects.all()

        return redirect('equipos/')

#----------------------------------------------------------




#----------------------------------------------------------
#Estadios
class estadios(generic.CreateView):
   template_name = 'home/estadios.html'
   model = models.Estadio
   form_class = forms.CreateEstadios
   success_url = '/estadios/'
   
   def get(self, request):
       estadios = models.Estadio.objects.all()
       context = {
           'Estadio': estadios,
           'form': self.form_class
       }
       
       return render(request, self.template_name, context)

class editarEstadios(generic.UpdateView):
    
    template_name = 'home/editarEstadios.html'
    model = models.Estadio
    form_class = forms.UpdateEstadios
    
    
    success_url = reverse_lazy("estadios")

class eliminarEstadios(generic.DeleteView):
    
    def get(self, request, pk):
        Estadios = models.Estadio.objects.get(id=pk)
        Estadios.delete()
        Estadios = models.Estadio.objects.all()

        return redirect('estadios/')

#----------------------------------------------------------



#----------------------------------------------------------
#Owner
class owner(generic.CreateView):
   template_name = 'home/owner.html'
   model = models.Owner
   form_class = forms.CreateOwner
   success_url = '/owner/'
   
   def get(self, request):
       owner = models.Owner.objects.all()
       context = {
           'Owner': owner,
           'form': self.form_class
       }
       
       return render(request, self.template_name, context)

class editarOwner(generic.UpdateView):
    
    template_name = 'home/editarOwner.html'
    model = models.Owner
    form_class = forms.UpdateOwner
    
    
    success_url = reverse_lazy("owner")

class eliminarOwner(generic.DeleteView):
    
    def get(self, request, pk):
        owner = models.Owner.objects.get(id=pk)
        owner.delete()
        owner = models.Owner.objects.all()

        return redirect('owner/')

#----------------------------------------------------------

