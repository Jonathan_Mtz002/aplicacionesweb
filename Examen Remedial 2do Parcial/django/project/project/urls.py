"""
URL configuration for project project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    
    path('jugadores/', views.jugadores.as_view(), name='jugadores'),
    path('editarJugador/<int:pk>/', views.editarJugador.as_view(), name='editarJugador'),
    path("eliminarJugador/<int:pk>/", views.eliminarJugador.as_view(), name='eliminarJugador'), 

    path('equipos/', views.equipos.as_view(), name='equipos'),
    path('editarEquipos/<int:pk>/', views.editarEquipos.as_view(), name='editarEquipos'),
    path("eliminarEquipos/<int:pk>/", views.eliminarEquipos.as_view(), name='eliminarEliminar'), 

    
    path('estadios/', views.estadios.as_view(), name='estadios'),
    path('editarEstadios/<int:pk>/', views.editarEstadios.as_view(), name='editarEstadios'),
    path("eliminarEstadios/<int:pk>/", views.eliminarEstadios.as_view(), name='eliminarEstadios'), 
    
    path('owner/', views.owner.as_view(), name='owner'),
    path('editarOwner/<int:pk>/', views.editarOwner.as_view(), name='editarOwner'),
    path("eliminarOwner/<int:pk>/", views.eliminarOwner.as_view(), name='eliminarOwner')
]
