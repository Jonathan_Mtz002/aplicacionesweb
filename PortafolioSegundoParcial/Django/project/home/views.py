from django.shortcuts import render, redirect
from .models import liga
# Create your views here.


def index(request):
    nfl = liga.objects.all()
    return render(request, "home/gestion.html", {"liga": nfl})

def registrarDatos(request):
    codigo = request.POST["txtCodigo"]
    equipo = request.POST["txtEquipo"]
    ciudad = request.POST["txtCiudad"]
    estadio = request.POST["txtEstadio"]
     
    nfl = liga.objects.create(codigo=codigo, equipo=equipo, ciudad=ciudad, estadio=estadio)
    return redirect("/")

def edicionDatos(request, codigo):
    nfl = liga.objects.get(codigo=codigo)
    return render(request, "editarDatos.html", {"liga": nfl})

def editarDatos(request):
    codigo = request.POST["txtCodigo"]
    equipo = request.POST["txtEquipo"]
    ciudad = request.POST["txtCiudad"]
    estadio = request.POST["txtEstadio"]
    
    nfl = liga.objects.get(codigo=codigo)
    nfl.equipo = equipo
    nfl.ciudad = ciudad
    nfl.estadio = estadio
    nfl.save()
    
    return redirect("/")

def eliminarDatos(request, codigo):
    nfl = liga.objects.get(codigo=codigo)
    nfl.delete()
    
    return redirect("/")

    